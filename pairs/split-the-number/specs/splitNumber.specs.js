(function () {
    'use strict';
    describe('nearsoft.pairprograming splitNumber', function() {
        var expect = require("chai").expect;
        var fs  = require("fs");
        var ns = require('../src/nearsoft.js');
        var input = fs.readFileSync('specs/input.txt').toString().split('\n');
        var output = fs.readFileSync('specs/output.txt').toString().split('\n');

        it('should exist', function(){
            expect(ns).to.have.property('splitNumber');
        });

        it('should match output', function(){
            input.forEach(function (lineInput, index) {
                var lineOutput = output[index];
                expect(ns.splitNumber(lineInput)).to.equal(lineOutput);
            });
        });
    });

}());