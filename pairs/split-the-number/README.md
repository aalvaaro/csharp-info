Nearsoft Pair Programing Test
==============
The purpose of this project is to evaluate a Nearsoft Candidate during the Pair Programing Test.

Candidate needs to solve the following algorithm using Javascript as a language. In case of candidate does not know how to program in javascript any other language can be used to solve it.

Prerequisites
----------------------------
NodeJs needs to be installed in order to run the tests.


Problem to Solve
----------------------------
You are given a positive number N and a pattern. The pattern consists of lowercase latin letters and one operation "+" or "-". The challenge is to split the number and evaluate it according to this pattern e.g. 
1232 ab+cd -> a:1, b:2, c:3, d:2 -> 12+32 -> 44

INPUT SAMPLE
3413289830 a-bcdefghij
776 a+bc
12345 a+bcde
1232 ab+cd
90602 a+bcde

OUTPUT SAMPLE
-413289827
83
2346
44
611

How To Run Tests
----------------------------
The following command is needed to download all dependencies:

```bash
npm install
```

This projects uses Grunt as a Task runner. To Run Specs just run the following command:
```bash
grunt test
```