using System.Collections.Generic;
using NUnit.Framework;

namespace Nearsoft.Test1
{
    [TestFixture]
    public class PlayerTest
    {
		[TestCase]
		public void PlayerAttributes() 
		{
			var player = new Player();
			Assert.IsNull(player.UserName);
			Assert.IsNull(player.FirstName);
			Assert.IsNull(player.LastName);
			Assert.AreEqual(0, player.TeamId);

			player.UserName = "doo@nearsoft.com";
			player.FirstName = "John";
			player.LastName = "Doe";
			var team = new Team();
			team.Id = 100;
			team.Name ="Baseball";
			player.Team = team;

			Assert.AreEqual("doo@nearsoft.com", player.UserName);
			Assert.AreEqual("John", player.FirstName);
			Assert.AreEqual("Doe", player.LastName);
			Assert.AreEqual(team.Id, player.Team.Id);
		}


		[TestCase]
		public void SortByUserNameAsc() 
		{
			Player playerFoo = new Player();
			playerFoo.UserName = "doo@nearsoft.com";

			Player playerBar = new Player();
			playerBar.UserName = "bar@nearsoft.com";

			var players = new List<Player>();
			players.Add(playerFoo);
			players.Add(playerBar);

			List<Player> sorted = Player.SortByUserNameAsc(players);
			Assert.AreEqual(2, sorted.Count);
			Assert.AreEqual("bar@nearsoft.com", sorted[0].UserName);
			Assert.AreEqual("doo@nearsoft.com", sorted[1].UserName);
		}

		[TestCase]
		public void SortByUserNameDesc() 
		{
			Player playerFoo = new Player();
			playerFoo.UserName = "doo@nearsoft.com";

			Player playerBar = new Player();
			playerBar.UserName = "bar@nearsoft.com";

			List<Player> players = new List<Player>();
			players.Add(playerFoo);
			players.Add(playerBar);

			List<Player> sorted = Player.SortByUserNameDesc(players);
			Assert.AreEqual(2, sorted.Count);
			Assert.AreEqual("doo@nearsoft.com", sorted[0].UserName);
			Assert.AreEqual("bar@nearsoft.com", sorted[1].UserName);
		}
    }
}