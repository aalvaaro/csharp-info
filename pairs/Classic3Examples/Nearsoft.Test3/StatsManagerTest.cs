using System;

namespace Nearsoft.Test3
{
    [TestFixture]
    public class StatsManagerTest
    {
        private StatsManager _statsManager;

        [SetUp]
        public void SetUp()
        {
            _statsManager = new StatsManager();
            _statsManager.LoadStatsFromFile("england_premier_league_2000.xml");
        }

        [TestCase]
        public void GetTeamWithMostWon_MustReturnBestTeam()
        {
            String expectedTeamName = "Manchester Utd";
            Assert.AreEqual(expectedTeamName, _statsManager.GetTeamNameWithMostWon());
        }

        [TestCase]
        public void GetTeamWithMostLost_MustReturnWorstTeam()
        {
            String expectedTeamName = "Bradford";
            Assert.AreEqual(expectedTeamName, _statsManager.GetTeamNameWithMostLost());
        }

        [TestCase]
        public void StatsToString_MustPrintSummaryResultTable()
        {
            String expectedSumary = LoadTextFile("summary_england_premier_league_2000.txt");
        }

        private string LoadTextFile(string path)
        {
            throw new NotImplementedException();
        }
    }
}