using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Nearsoft.Test2
{
    [TestFixture]
    public class PlayerRepositoryTest
    {
        private SqlConnection _connection;

        [SetUp]
        public void SetUp()
        {
            _connection = GetConnection();
        }

        private SqlConnection GetConnection()
        {
            throw new NotImplementedException();
        }

        [TestCase]
        public void GetPlayers_MustReturnAllPlayers()
        {
            var playerRepository = new PlayerRepository(_connection);
            List<Player> players = playerRepository.GetPlayers();
            Assert.AreEqual(3, players.Count);

            Player john = players[0];
            Assert.AreEqual("john@nearsoft.com", john.UserName);
            Assert.AreEqual("John", john.FirstName);
            Assert.AreEqual("Doe", john.LastName);
            Assert.AreEqual(100, john.TeamId);

            Player jane = players[1];
            Assert.AreEqual("jane@nearsoft.com", jane.UserName);
            Assert.AreEqual("Jane", jane.FirstName);
            Assert.AreEqual("Doe", jane.LastName);
            Assert.AreEqual(100, jane.TeamId);

            Player bob = players[2];
            Assert.AreEqual("bob@nearsoft.com", bob.UserName);
            Assert.AreEqual("Bob", bob.FirstName);
            Assert.AreEqual("Ross", bob.LastName);
        }

        [TestCase]
        public void GetPlayersByTeamId_WithValidTeamId_MustReturnTeamMembers()
        {
            var playerRepository = new PlayerRepository(_connection);
            List<Player> players = playerRepository.GetPlayersByTeamId(100);
            Assert.AreEqual(2, players.Count);

            Player john = players[0];
            Assert.AreEqual("john@nearsoft.com", john.UserName);
            Assert.AreEqual("John", john.FirstName);
            Assert.AreEqual("Doe", john.LastName);
            Assert.AreEqual(100, john.TeamId);

            Team softball = john.Team;
            Assert.AreEqual(100, softball.Id);
            Assert.AreEqual("Softball", softball.Name);

            Player jane = players[0];
            Assert.AreEqual("jane@nearsoft.com", jane.UserName);
            Assert.AreEqual("Jane", jane.FirstName);
            Assert.AreEqual("Doe", jane.LastName);
            Assert.AreEqual(100, jane.TeamId);

            Assert.AreEqual(softball, jane.Team);
        }
    }
}