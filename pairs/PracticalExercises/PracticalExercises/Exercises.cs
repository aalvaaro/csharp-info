﻿using System;
using System.Linq;
using System.Text;

namespace PracticalExercises
{
    public class Exercises
    {

        /*
         * Exercise 1
         * Given two arrays, 1,2,3,4,5 and 2,3,1,0,5 find which number is not present in the second array.
         */
        public int FindNumberThatIsNotPresent(int[] first, int[] second)
        {
            throw new NotImplementedException();
        }
        /*
         * Exercise 2
         * The sum of the squares of the first ten natural numbers is,
         * 
         * 1^2 + 2^2 + ... + 10^2 = 385
         * 
         * The square of the sum of the first ten natural numbers is,
         * 
         * (1 + 2 + ... + 10)^2 = 55^2 = 3025
         * 
         * Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 - 385 = 2640.
         * 
         * Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
         */
        public double FindDifferenceSumOfSquaresAndSquareOfSums(int upTo)
        {
            throw new NotImplementedException();
        }

        /*
         * Exercise 3
         * Given the input string, write a program to reverse the words for the supplied input string.
         * 
         * Input: "The quick brown fox jumps over the lazy dog"
         * 
         * Output: “dog lazy the over jumps fox brown quick The”
         */
        public string ReverseWordsInString(string s)
        {
            throw new NotImplementedException();
        }

        /*
         * Exercise 4
         * Number pyramid Write a program that accepts an input parameter that is a number (n). Given this input number (n), print a pyramid that starts with 1 at the top and outputs each successive number in a pyramid up to (n).
         * 
         * Input: 
         *    5
         * 
         * Output: 
         *     1 
         *    121 
         *   12321 
         *  1234321 
         * 123454321
         */
        public string NumberToPyramid(int number)
        {
            throw new NotImplementedException();
        }

        /*
         * Exercise 5
         * Sort Array Given an array of input strings, write a program that sorts (alphabetically) the array of input strings and capitalizes the first and last letter of each word.
         * 
         * Input: 
         *
         *    “cat”
         *    “animal”
         *    “abacus”
         *    “framework”
         * 
         * Output:
         *
         *    “AbacuS”
         *    “AnimaL”
         *    “CaT”
         *    “FrameworK”
         */
        public string[] SortArrayOfStrings(string[] array)
        {
            throw new NotImplementedException();
        }

    }
}
