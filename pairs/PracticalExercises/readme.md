# Practical Exercises

This is a collection of exercises that test logical reasoning and knowledge about the C# programming language.


### Requirements:

- .Net Core 2.0 Framework
- Visual Studio or Visual Studio Code

### This exercise measures knowledge of:
- Logical Reasoning
- .Net and C# knowledge
- Knowledge of SQL
- Unit Testing
- Teamwork


## Task:

Wrtie a solution for each one of exercises below.


### Exercise 1

Given two arrays, `1,2,3,4,5` and `2,3,1,0,5` find which number is not present in the second array.


### Exercise 2

The sum of the squares of the first ten natural numbers is,

 `1^2 + 2^2 + ... + 10^2 = 385`

The square of the sum of the first ten natural numbers is,

 `(1 + 2 + ... + 10)^2 = 55^2 = 3025`

Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is `3025 - 385 = 2640`.

Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.

### Exercise 3

Given the input string, write a program to reverse the words for the supplied input string.

`Input: "The quick brown fox jumps over the lazy dog"`

`Output: “dog lazy the over jumps fox brown quick The”`

### Exercise 4

Number pyramid
Write a program that accepts an input parameter that is a number (n).  Given this input number (n), print a pyramid that starts with 1 at the top and outputs each successive number in a pyramid up to (n).

```
Input: 
    5
```

```
Output: 

    1
   121
  12321
 1234321
123454321
```

### Exercise 5

Sort Array
Given an array of input strings, write a program that sorts (alphabetically) the array of input strings and capitalizes the first and last letter of each word.

```
Input: 

    “cat”
    “animal”
    “abacus”
    “framework”`
```

```
Output:

    “AbacuS”
    “AnimaL”
    “CaT”
    “FrameworK”
```

### Exercise 6

What is wrong with this program?  How would you make it better?

```C#
using System;
using System.Data;
using System.Data.SqlClient;

namespace WhatIsWrong
{
    class Program
    {
        static void Main(string[] args)
        {
            SqlConnection myConnection = new SqlConnection("<some connection string>");

            SqlCommand myCommand = new SqlCommand("<some stored procedure>", myConnection);

            myCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter parameterItemID = new SqlParameter("@ItemID", SqlDbType.Int, 4);

            parameterItemID.Value = 1;
            myCommand.Parameters.Add(parameterItemID);

            myConnection.Open();

            myCommand.ExecuteNonQuery();

            myConnection.Close();
        }
    }
}
```


### Exercise 7

Assuming the following basic  table structure

    - Songs (SongID : int, Name: varchar, ReleaseDate: date)
    - Tags (TagID : int, Name: varchar)
    - SongsTags (SongID: int, TagID: int)

*Columns with ID in their name are table keys.*


Write the following queries:

    - Songs with a ReleaseDate on 1/1/2010 or after  
    - Songs that are tagged "Rock"  
    - Songs that are tagged either "Rock" or "Country"
    - Songs that are tagged "Rock" and "Country"


## Possible Solution (Don't show this to the candidate)


### Exercise 1

```C#
public int FindNumberThatIsNotPresent(int[] first, int[] second)
{
    var res = first.FirstOrDefault(n => !second.Contains(n));
    return res;
}
```

### Exercise 2

```C#
public double FindDifferenceSumOfSquaresAndSquareOfSums(int upTo)
{
    var sumOfSquares = 0.0;
    var sum = 0.0;
    for (var i = 1; i <= upTo; i++)
    {
        sumOfSquares += Math.Pow(i, 2);
        sum += i;
    }

    var squareOfSum = Math.Pow(sum, 2);

    return squareOfSum - sumOfSquares;
}
```

### Exercise 3

```C#
public string ReverseWordsInString(string s)
{
    var strings = s.Split(' ');

    var reversed = strings.Reverse();
    var res = string.Join(' ', reversed);

    return res;
}
```

### Exercise 4

```C#
public string NumberToPyramid(int number)
{
    var builder = new StringBuilder();

    for (var i = 1; i <= number; i++)
    {
        var numberString = CreateNumberString(i);
        var padding = new string(' ', number - i);

        builder.AppendLine($"{padding}{numberString}");
    }


    return builder.ToString();
}

string CreateNumberString(int n)
{
    var builder = new StringBuilder();
    for (int i = 1; i <= n; i++)
    {
        builder.Append(i);
    }
    for (int i = n - 1; i > 0; i--)
    {
        builder.Append(i);
    }
    return builder.ToString();
}

```

### Exercise 5

```C#
public string[] SortArrayOfStrings(string[] array)
{
    var sorted = array.OrderBy(s => s);
    var capitalized = sorted.Select(s => FormatString(s));
    return capitalized.ToArray();
}

private string FormatString(string s)
{
    var res = new StringBuilder();

    for (var i = 0; i < s.Length; i++)
    {
        var c = s[i];

        if ((i == 0) || (i == s.Length - 1))
        {
            c = char.ToUpper(c);
        }

        res.Append(c);
    }

    return res.ToString();
}
```

### Exercise 6

```
- Make sure to close the connection even if there are exceptions. 
- Use a using statement to automatically close the connection.
```

### Exercise 7

Query #1
```SQL
SELECT Name FROM Songs WHERE ReleaseDate >= '2010/1/1'
```

Query #2
```SQL
SELECT s.Name FROM Songs s
JOIN SongsTags st ON 
    ( s.SongId = st.SongId )
JOIN Tags t ON
    (st.TagId = t.TagId)
WHERE t.Name = 'Rock'
```

Query #3
```SQL
SELECT DISTINCT s.Name FROM Songs s
JOIN SongsTags st ON 
    ( s.SongId = st.SongId )
JOIN Tags t ON
    (st.TagId = t.TagId)
WHERE t.Name = 'Rock' or t.Name = 'Country'
```

Query #4
```SQL
(SELECT s.Name FROM Songs s
JOIN SongsTags st ON 
    ( s.SongId = st.SongId )
JOIN Tags t ON
    (st.TagId = t.TagId)
WHERE t.Name = 'Rock') 

INTERSECT

(SELECT s.Name FROM Songs s
JOIN SongsTags st ON 
    ( s.SongId = st.SongId )
JOIN Tags t ON
    (st.TagId = t.TagId)
WHERE t.Name = 'Country')
```



You can use [SqlFiddle](http://sqlfiddle.com/) to test the queries and create the schema using the script below.

```SQL
CREATE TABLE [dbo].[Songs](
    [SongId] [int] NULL,
    [Name] [varchar](64) NULL,
    [ReleaseDate] [DATE] NULL,
) 
CREATE TABLE [dbo].[Tags](
    [TagId] [int] NULL,
    [Name] [varchar](64) NULL,
) 
CREATE TABLE [dbo].[SongsTags](
    [SongId] [int] NULL,
    [TagId] [int] NULL
) 

INSERT INTO Songs (SongId, Name, ReleaseDate) VALUES (1, 'One', '1992/3/6')
INSERT INTO Songs (SongId, Name, ReleaseDate) VALUES (2, 'Yellow', '2000/6/26')
INSERT INTO Songs (SongId, Name, ReleaseDate) VALUES (3, 'Begin Again', '2014/7/1')
INSERT INTO Songs (SongId, Name, ReleaseDate) VALUES (4, '22', '2013/3/5')

INSERT INTO Tags (TagId, Name) VALUES (1, 'Rock')
INSERT INTO Tags (TagId, Name) VALUES (2, 'Country')

INSERT INTO SongsTags (SongId, TagId) VALUES (1, 1)
INSERT INTO SongsTags (SongId, TagId) VALUES (2, 1)
INSERT INTO SongsTags (SongId, TagId) VALUES (3, 1)
INSERT INTO SongsTags (SongId, TagId) VALUES (3, 2)
INSERT INTO SongsTags (SongId, TagId) VALUES (4, 2)

```
