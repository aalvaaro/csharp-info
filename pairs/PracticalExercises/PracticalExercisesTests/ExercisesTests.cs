using System;
using PracticalExercises;
using Xunit;

namespace PracticalExercisesTests
{
    public class ExercisesTests
    {
        [Fact]
        public void FindNumberThatIsNotPresentTest()
        {
            var e = new Exercises();

            var res = e.FindNumberThatIsNotPresent(new int[]{1,2,3,4,5}, new int[] { 2, 3, 1, 0, 5 });

            Assert.Equal(4, res);
        }

        [Fact]
        public void FindDifferenceSumOfSquaresAndSquareOfSumsTest()
        {
            var e = new Exercises();

            var res = e.FindDifferenceSumOfSquaresAndSquareOfSums(100);

            Assert.Equal(25164150, res);
        }


        [Fact]
        public void ReverseWordsInStringTest()
        {
            var e = new Exercises();

            var res = e.ReverseWordsInString("The quick brown fox jumps over the lazy dog");

            Assert.Equal("dog lazy the over jumps fox brown quick The", res);
        }

        [Fact]
        public void NumberToPyramidTest()
        {
            var e = new Exercises();

            var res = e.NumberToPyramid(5);

            Assert.Equal($"    1\n   121\n  12321\n 1234321\n123454321\n", res);
        }

        [Fact]
        public void SortArrayOfStringsTest()
        {
            var e = new Exercises();

            var input = new string[] { "cat", "animal", "abacus", "framework" };
            var res = e.SortArrayOfStrings(input);
            var expected = new string[] { "AbacuS", "AnimaL", "CaT", "FrameworK" };

            Assert.Equal(expected, res);
        }
    }
}
