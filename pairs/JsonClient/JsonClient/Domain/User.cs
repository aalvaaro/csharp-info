﻿using System;
namespace JsonClient.Domain
{
    public class User
    {
        public User()
        {
            
        }

        public string Name { get; set; }
        public Address Address { get; set; }
        public string Phone { get; set; }
    }
}
