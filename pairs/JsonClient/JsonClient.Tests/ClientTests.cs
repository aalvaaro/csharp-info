using System;
using System.Linq;
using System.Collections.Generic;
using JsonClient.Domain;
using Xunit;

namespace JsonClient.Tests
{
    public class ClientTests
    {
        const string postsUrl = "https://jsonplaceholder.typicode.com/posts";

        [Fact]
        public void Client_Gets_Post_By_Id_Correctly()
        {
            var client = new Client<Post>(postsUrl);

            var post = client.Get(1);
            Assert.NotNull(post);
            Assert.IsType<Post>(post);
            Assert.Equal(1, post.Id);
            Assert.Equal("sunt aut facere repellat provident occaecati excepturi optio reprehenderit", post.Title);
        }

        [Fact]
        public void Client_Gets_All_Posts_Correctly()
        {
            var client = new Client<Post>(postsUrl);

            var posts = client.GetAll();
            Assert.NotNull(posts);
            Assert.IsType<List<Post>>(posts);
            Assert.Equal(100, posts.Count());
        }
    }
}
