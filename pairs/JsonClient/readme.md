# JsonClient

JsonClient is an exercise to be solved during a pair programming session. The goal of the exercise is to write a generic client for any web service that returns a resource in JSON following the REST way and also 
implement an generic object writer that can print to console all the properties in the objects returned from the web service. 
The solution is a console application written in C# in the .Net Core 2.0 framework.

### Requirements:
 - .Net Core installed in your computer.
 - Visual Studio or Visual Studio Code.

### Tasks:
 - Make the solution build correctly.
 - Write a generic JSON Client
   - Implement the Client class inside the JsonClient project.
     - Get and GetAll methods should also serialize properties that are not primitive (Nested objects).
     - Client's constructor should not allow null or empty values as the baseUrl.
   - Make the 2 unit tests inside the ClientTests class pass.
   - Write 1 extra unit test which checks that the Client constructor throws an exception when instantiating in it with a null or empty value for the baseUrl parameter.
   - Write a set of tests for the Client class that consumes the [User](https://jsonplaceholder.typicode.com/users) resource from [JSONPlaceholder](https://jsonplaceholder.typicode.com/). 
 - Write a generic Console Object Writer
   - Implement the ConsoleObjectWriter class inside the JsonClient project.
   - The Object Writer should write all properties of an onbject to the console.
   - The Object Writer should be generic and work with any class.
   - Write 2 unit tests for Object Writer.
     - Check that it can handle a null instance and doesn't throw an exception.
     - Check that it prints all public properties of an object to console.
 - Use the Generic Object Writer to print the contents of the generic JSON Client's response when getting resources from [JSONPlaceholder](https://jsonplaceholder.typicode.com/). 
   - Print the response of getting one object by its Id.
   - Print the response of getting all objects of a resource.
   - Print the response of getting one object but its Id. The gotten object should contain a nested class type (Ex. [User](https://jsonplaceholder.typicode.com/users)).


#### Hint: Use one of the classes in the .Net standard network library to complete the exercise.


### This exercise measures knowledge of:
  - Logical Reasoning 
  - .Net and C# knowledge
  - Network, HTTP and REST knowledge
  - Design Patterns
  - Inheritance vs Composition
  - Unit Testing
  - Generics
  - Teamwork
 
 
 ## Possible Solution (Don't show this to the candidate)

 #### Client.cs

 ```C#
using System;
using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;

namespace JsonClient
{
    public class Client<T>
    {
        string baseUrl { get; set; }

        public Client(string baseUrl)
        {
            this.baseUrl = baseUrl;
        }

        public T Get(int id)
        {
            using (var client = new WebClient())
            {
                var json = client.DownloadString(baseUrl + $"/{id}");

                var res = JsonConvert.DeserializeObject<T>(json);
                return res;
            }
        }

        public List<T> GetAll()
        {
            using (var client = new WebClient())
            {
                var json = client.DownloadString(baseUrl);

                var res = JsonConvert.DeserializeObject<List<T>>(json);
                return res;
            }
        }
    }
}
 ```

 #### ConsoleObjectWriter.cs

 ```C#
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace JsonClient
{
    public class ConsoleObjectWriter
    {
        TextWriter writer;
        public ConsoleObjectWriter(TextWriter writer)
        {
            this.writer = writer;
        }

        public void WriteLine(string s)
        {
            writer.WriteLine(s);
        }

        public void WriteLine(object o)
        {
            var builder = new StringBuilder();

            appendProperties(o, builder);

            WriteLine(builder.ToString());
        }

        private void appendProperties(object o, StringBuilder builder){
            var type = o.GetType();
            var properties = type.GetProperties().Where(p => p.GetIndexParameters().Length == 0).ToList();

            builder.Append($"[ {type.Name}: [");
            foreach (var p in properties)
            {
                var val = p.GetValue(o, null);
                var t = val.GetType();
                if (t.IsPrimitive || t == typeof(Decimal) || t == typeof(String))
                {
                    builder.Append($" {p.Name}: {val}");
                }
                else{
                    appendProperties(val, builder);
                }
            }
            builder.Append("]");
        }


        public void WriteLines(List<object> objects)
        {
            foreach(var o in objects){
                WriteLine(o);
            }
        }
    }
}

 ```

 ### Program.cs

 ```C#
using System;
using System.Linq;
using JsonClient.Domain;

namespace JsonClient
{
    class Program
    {
        static void Main(string[] args)
        {
            const string postsUrl = "https://jsonplaceholder.typicode.com/users";
            var client = new Client<User>(postsUrl);

            var post = client.GetAll();

            var writer = new ConsoleObjectWriter(Console.Out);
            writer.WriteLines(post.Select(p=> (object)p).ToList());
        }
    }
}

 ```