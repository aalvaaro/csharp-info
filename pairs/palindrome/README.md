## Palindrome

**Duration: `15 min`**

Write a function that checks if a given sentence is a palindrome.
> A palindrome is a word, number, phrase, verse, or sentence that reads the same backward or forward.

#### Caveats: 
* Ignore all punctuation and space characters. 
* By design the strings in .NET are immutable: Do not allocate or instantiate any new strings.

**Tip**: Ignore all the caveats first and the try to apply them.

### Initial Project

```csharp
using System;
using System.Linq;

namespace DotNet.Exercises.Palindrome
{
    public class Program
    {
        public static bool IsPalindrome(string value)
        {
            // start here
            
            return true;
        }

        public static void Main(string[] args)
        {
            var sentences = new[] {
                /*true */ "radar",
                /*false*/ "pato",
                /*true */ "civic",
                /*true */ "madam",
                /*true */ "A man, a plan, a canal: Panama.",
                /*true */ "Dammit, I'm mad!",
                /*true */ "...",
                /*true */ "",
                /*false*/ "Palindrome" };

            foreach(var text in sentences) {
                Console.WriteLine($"'{text}' : {IsPalindrome(text)}");
            }

            Console.ReadKey();
        }
    }
}
```

### The solution

There's a possible solution in the repo, to run it:

```sh
cd Solution
dotnet run
```
