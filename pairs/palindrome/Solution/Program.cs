using System;
using System.Linq;

namespace DotNet.Exercises.PalindromeProgram
{
    public class Program
    {
        public static bool IsPalindrome(string value)
        {
            int min = 0;
            int max = value.Length - 1;
            while (true)
            {
                if (min > max)
                {
                    return true;
                }
                char a = value[min];
                char b = value[max];

                // using two sub-loops to skip back and forward to avoid allocate any new strings

                // Scan forward for 'a' while invalid.
                while (!char.IsLetterOrDigit(a))
                {
                    min++;
                    if (min > max)
                    {
                        return true;
                    }
                    a = value[min];
                }

                // Scan backward for 'b' while invalid.
                while (!char.IsLetterOrDigit(b))
                {
                    max--;
                    if (min > max)
                    {
                        return true;
                    }
                    b = value[max];
                }

                // Instead of converting the input string with ToLower, which would require an allocation, 
                // we check the lowercased versions of all the characters. This makes it faster.
                if (char.ToLower(a) != char.ToLower(b))
                {
                    return false;
                }
                min++;
                max--;
            }
        }

        public static void Main(string[] args)
        {
            var sentences = new[] {
                /*true */ "radar",
                /*false*/ "pato",
                /*true */ "civic",
                /*true */ "madam",
                /*true */ "A man, a plan, a canal: Panama.",
                /*true */ "Dammit, I'm mad!",
                /*true */ "...",
                /*true */ "",
                /*false*/ "Palindrome" };

            sentences.ToList().ForEach(x => Console.WriteLine($"'{x}' : {IsPalindrome(x)}"));

            Console.ReadKey();
        }
    }
}
