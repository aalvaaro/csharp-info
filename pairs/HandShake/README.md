## Hand Shake Problem

**Duration: `15 min`**

Write a program to calculate the number of handshakes during an event given `N`, where `N` is the number of people in the event.

Assume that everyone gave a handshake.

#### Caveats: 
* Any solution is valid.
* Try to write clean code.
* The program function should receive a list of `N` numbers and print a sentence like:

> The total number of handshakes for 10 people is: {total}

### Initial Project

```csharp
using System;
using System.Collections.Generic;

namespace Combinations
{
    public class Program
    {
        private static void Main(string[] args)
        {
            var numberPeople = new List<int> {4, 5, 6, 7, 8, 15};
            foreach (var n in numberPeople)
            {
                Console.WriteLine($"The total number of handshakes for {n} people is: {FunctionToRetrieveTheTotalNumberOfHandShakes(n)}");
            }

            Console.ReadLine();
        }        
    }
}

```

### The Solution

There's a possible solution in the repo. To run it:

```sh
cd Solution
dotnet run
```
