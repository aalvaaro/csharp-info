﻿using System;
using System.Collections.Generic;

namespace Combinations
{
    public class Program
    {
        private static void Main(string[] args)
        {
            var numberPeople = new List<int> {4, 5, 6, 7, 8};
            foreach (var n in numberPeople)
            {
                Console.WriteLine($"The total of HandShaking for {n} persons are: {Combination(n)}");
            }

            Console.ReadLine();
        }

        private static int Combination(int n)
        {
            var totalCombinations = 0;
            const int r = 2; // HandShake between 2 persons.
            var k = n - r;
            
            // Comb = Fact(n) / Fact(r) * (Fact(n-r))
            totalCombinations = Factorial(n)/(Factorial(r)*Factorial(k));

            return totalCombinations;
        }

        private static int Factorial(int n)
        {
            var fact = 1;
            for (var i = 1; i <= n; i++)
            {
                fact = fact * i;
            }
            return fact;
        }
    }
}
