dotnet
=======

Nearsoft .NET Community

## Interviews

> Interviews are a large part of the developer hiring process. So what can you do to make the process better for everyone **involved**?
> Below are some documents and tips from some of our developers. Please take a moment to review every document here and feel free to ask
> about how to contribute to this repo.

### Differences in skill level

According to the [Dreyfus model of skill acquisition](https://en.wikipedia.org/wiki/Dreyfus_model_of_skill_acquisition) we have identified the following four levels of skills in programming and .Net in particular. The following overview can help you understand the different levels in skills.

<table>
<thead>
 <tr>
  <th>Rating</th>
  <th>Description</th>
 <tr>
</thead>
<tbody>
 <tr>
  <td> <b>Novice</b> <br> <i>You’ve just begun to learn about the aspects and principles of the subject. If this is an area of interest for you, then this is likely an opportunity for growth.</i></td>
  <td> 
        Culture-fit, Self-learning, Commitment, Proactivity, Enthuasiasm, Self-motivated. The candidate has performed simple tasks given instruction or guidance.
    <br><br>The candidate knows about programming concepts in general (Polymorphism, Encapsulation, Abstract Classes, etc.) Basic things about programming that are cross language such as access modifiers, method overloading, etc. 
    He/She also knows about data structures and common algorithms. Knowledge is not deep.     
  </td>
 </tr>
 <tr>
  <td> <b>Begginer</b> <br> <i>You have enough skill to be successful in your current role. You understand aspects, principles, and guidelines of the subject.</i></td>
  <td> 
        Good communicator, Technically Competent. The candidate has performed simple tasks independently.
    <br><br>The candidate knows about common concepts specific to C# such as Extension Methods, Namespaces, LINQ, and most of the procedural programming concepts suach read only fields, const fields and Generics. He/She can solve programming exercises without help.
  </td>
 </tr>
 <tr>
  <td> <b>Intermediate</b> <br> <i>You know how to use the right stuff at the right time. You can rationalize why something should be completed a certain way. You're able to apply the concepts in many different contexts, and reason about where they work well, and where they don't.</i></td>
  <td> 
        Proficient with Tools, Precise, Handle and Work with Uncertainty. The candidate has performed complex tasks, diagnosed issues, determined root-causes and made changes to resolve them.  
    <br><br>The candidate knows advanced topics of programming and OOP such as when to use composition over inheritance. How to use Async programming and what expression trees are. Knows and understands what Design Patterns are. Knows about Unit Testing.
  </td>
 </tr>
 <tr> 
  <td> <b>Advanced</b> <br> <i>You’re an expert- you can do this in your sleep. You have an intuitive grasp and deep understanding of the subject and take an active role in teaching others. You have a vision for what’s possible.</i></td>
  <td> 
        Approach definer, Mentoring, Effectiveness, Innovation. The candidate has demonstrated advanced knowledge of programming in previous jobs and has taught others about the subject.
    <br><br>The candidate has deep understanding of OOP and Design Paterns (IoC, SOLID, DRY, KISS). Knows about frameworks for DI and has used them to improve the structure and maintainability of applications. Knows about different programming paradigms such as AOP and Functional programming and knows how they work and when to use them. He/She knows about application architecture and design.
  </td>
 </tr>
<tbody>
</table>



Reference [Dreyfus model of skill acquisition](https://en.wikipedia.org/wiki/Dreyfus_model_of_skill_acquisition)

### How to check for Cultural Fit in an interview

Please see this document [How to check for Cultural Fit in an interview] (https://docs.google.com/spreadsheets/d/1PgzqVUVxbse78wYKmAKXVmP8htqKTxGgrvZs5CP5-x8/edit#gid=0)

### Recruiting Rewards

At Nearsoft we have rewards if you participate as a interviewer. Please review this document [Recruiting Rewards] (https://docs.google.com/document/d/1GQ-kfJWPybaQDk7W8TgXW378EltEOyWuMYHE5wat8jA/edit) to see the rewards you can get from recruitment department.

If you has been participated as a interviewer and you want to know about the participation during the year, please see this document [Participation] (https://docs.google.com/spreadsheets/d/1EfmpRx3bJZG30l37VZJJxPVO5Q86G8mvx06GYpxfs2w/edit#gid=0)


### Nearsoft Employee Referral Program
 Please see this document [Referral Program] (https://docs.google.com/document/d/1kkU6GVUmTVBemGWKvt-F30gNA1OqJ7k5E3hAgqNW1W8/edit)

## Pair Exercises

## Training

## Tips during the interview

- Please see [Stack Overflow Guide] (http://cdn2.hubspot.net/hubfs/450622/Guide_to_Interviewing_Developers.pdf)
- Review [The Most Important Interview Question of All Time] (https://www.google.com/url?hl=en&q=http://louadlergroup.com/the-most-important-interview-question-of-all-time/&source=gmail&ust=1473817028566000&usg=AFQjCNFu_bae4YTqBN9fxZB-ZTX7HxOkNg)
- Be prepared for the interview. Including being informed of who they will be speaking with.
- During the interview we should first ask the candidate about his/her experience. At this point, you should let him/her talk about everything.

