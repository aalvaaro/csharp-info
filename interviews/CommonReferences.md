# .NET COMMON REFERENCES

### C# Guía de Programación
[Microsoft Guide] (https://msdn.microsoft.com/es-es/library/kx37x362(v=vs.80).aspx)

### OOP
- 

### OOD
- [Principles of OOD] (http://butunclebob.com/ArticleS.UncleBob.PrinciplesOfOod)

### Design Patterns
- [Head First Book] (http://shop.oreilly.com/product/9780596007126.do)

### Algorithms and Big-O Notation

- [Introduction to Algorithms] (https://www.amazon.com/Introduction-Algorithms-3rd-MIT-Press/dp/0262033844/ref=sr_1_1?s=books&ie=UTF8&qid=1472492668&sr=1-1&keywords=Corman)
- [LinkedList vs ArrayList] (http://stackoverflow.com/questions/322715/when-to-use-linkedlist-over-arraylist)
- [Big O Notation] (http://www.enrique7mc.com/2016/07/que-es-la-notacion-big-o/)

### Javascript
- [Mozilla Developer Network] (https://developer.mozilla.org/en-US/docs/Web/JavaScript)
- [Eloquent Book] (http://eloquentjavascript.net)
- [Function Invocation Patterns] (http://doctrina.org/Javascript-Function-Invocation-Patterns.html)

### HTML Y CSS
- [Html&CSS design and build websites] (http://wtf.tw/ref/duckett.pdf)

### Recommended Books
- [The pragmation programmer] (https://www.amazon.com/The-Pragmatic-Programmer-Journeyman-Master/dp/020161622X/ref=pd_bxgy_14_img_2?ie=UTF8&refRID=1RWP5CA54MCXZGSJZM9P)

### Base de Datos
