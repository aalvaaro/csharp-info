### .NET INTERVIEW GUIDE
> https://docs.google.com/document/d/1MH4So2Ls0o83BdKvrXxyDcpZw1utywyyL3TFRgb8r1o/edit

## OOP (Basic)
1. Which are the pillars of the OOP. (Inheritance, Encapsulation, Polymorphism, Abstraction)
1. What is a class and what is an object.
1. Access Modifies C#
1. Nullable Types
1. How to prevent a class to be inherited? Sealed
1. What is the default access modifier to a class? Internal
1. What is the default access modifier to methods? Private
1.  Overloading vs. Override
1. Override vs. Shadowing
1. Partial class
1. Difference between instance methods and class methods.
1. Readonly Fields vs. Const fields.
1. Static Class vs. Non-static class
1. Can you have multiple catch blocks for a try statement? If so, in what order are executed?
1. Abstract Class vs. Interface
1. Scenarios when use an abstract class vs. interface.
1. Extension Methods
1. Boxing vs Unboxing 
1. Generics

## Avanzado .NET
1. Extension Methods
1. Constraint Generics
1. Composition vs Inheritance
1. Async Programming
1. Anonymous functions
1. Delegates and events
1. Lambda Expressions.
1. Expression Tree.
1. Multicasting.
1. Reflection
1. Stack vs Heap. Managed memories
1. Memory Leak 
1. LINQ
1. IEnumerable vs IQueryable - (https://stackoverflow.com/a/28513685)
1. Variable por referencia o por valor
 
## OOD:
1. SOLID
1. DRY, KISS, YAGNI
1. IoC => Inversion of Control
1. Design Patterns ->  Mencionar un par y describir cómo lo has usado.
 
### DATA STRUCTURES
1. Mention some structures that you know
1. LinkedList vs. Double Linked List
1. Queue, Stack
1. In C# the List, what kind of structure use. Double Linked List
1. Mention the sorting algorithms you know.
1. O (n).
 
### SQL
1. Normalization
1. Primary Key
1. Clustered Index vs. Non-Clustered Index
1. Inner Join vs. Left Join
1. Difference between View and SP.
1. Mention some aggregate functions. AVG, SUM, MIN, MAX
1. What you should do when a stored procedure is taking long time to return a result.
1. Execution Plan
1. Deadlock
1. What is SQL Injection and how do you guard against it?
1. How can you stop a DBA from making off with a list of users’ passwords? Hash them

### TESTING & QUALITY
1. TFS, GIT, SVN
1. CI (Jenkins, TFS, TeamCity)
1. Unit Testing (server and client side)

### AGILE
1. Tools for sprint management
1. How you can solve a problematic where are different developers in different locations.
 
### FRONT END
1. Difference between p y span
1. Box model
1. DOCTYPE
1. Types of selectors in CSS
1. Function Declaration vs. Function Expression
1. What is the scope in JavaScript?
1. What is a closure in JavaScript?
1. What is a prototype in JavaScript?
1. What is a prototype chain?
1. Hoisting
1. Any framework you use in the front end. (AngularJs, React, EmberJs,  Knockout, Meteor, Aurelia)
1. BreezeJs
 
### SOURCE CONTROL
1. What source control you usually use?
1. Could you please tell me any branching strategy? Central Strategy, Feature Strategy
1. What types of source control systems exist? Distributed and Centralized (Git, SVN)
 
## CULTURAL FIT
1. Qué es lo que sabes de NS y porque quisieras entrar?
1. Cuales son tus expectativas si entras a NS??
1. Qué haces si ves alguna problemática en alguna área de la compañía que pudieras mejorar??
1. Cómo te defines a ti mismo en cuestión de liderazgo y hacer que las cosas sucedan?
1. Cómo te mantienes actualizado??
1. Como te mantienes motivado para seguir aprendiendo dia a dia?
